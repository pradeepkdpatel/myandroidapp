package com.patel.pradeep.mydatepickerapplication;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.TextView;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;

import java.text.DateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    //private ActionBarDrawerToggle actionBarDrawerToggle;

    TextView from;
    TextView to;
    TextView result;

    Calendar fromCalendar = null;
    Calendar toCalendar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        from = (TextView) findViewById(R.id.from);
        to = (TextView) findViewById(R.id.to);
        result = (TextView) findViewById(R.id.result);

        drawerLayout = (DrawerLayout)findViewById(R.id.drawer);
//        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
//        drawerLayout.addDrawerListener(actionBarDrawerToggle);
//        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nav_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*if(actionBarDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }*/

        if(item.getItemId() == R.id.action_home){
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            return true;
        }
        if(item.getItemId() == R.id.action_date){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void fromShowDPD(View v) {
        Calendar cal = Calendar.getInstance();
        int curr_day = cal.get(Calendar.DAY_OF_MONTH);
        int curr_month = cal.get(Calendar.MONTH);
        int curr_year = cal.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //from.setText("From Date: " + dayOfMonth + "/" + (month + 1) + "/" + year);
                Calendar cal = Calendar.getInstance();
                cal.set(year,month,dayOfMonth);
                DateFormat dateFormat = DateFormat.getDateInstance();
                from.setText("From Date: " +  dateFormat.format(cal.getTime()));
                fromCalendar = Calendar.getInstance();
                fromCalendar.set(year,month,dayOfMonth);
            }
        }, curr_year, curr_month, curr_day);
        datePickerDialog.show();
    }

    public void toShowDPD(View v) {
        Calendar cal = Calendar.getInstance();
        int curr_day = cal.get(Calendar.DAY_OF_MONTH);
        int curr_month = cal.get(Calendar.MONTH);
        int curr_year = cal.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(year,month,dayOfMonth);
                DateFormat dateFormat = DateFormat.getDateInstance();
                to.setText("To Date: " +  dateFormat.format(cal.getTime()));
                toCalendar = Calendar.getInstance();
                toCalendar.set(year,month,dayOfMonth);
            }
        }, curr_year, curr_month, curr_day);
        datePickerDialog.show();
    }

    public void showDateDiff(View v) {
        if(fromCalendar == null){
            result.clearComposingText();
            result.setText("Select From Date!");
        }else if(toCalendar == null){
            result.clearComposingText();
            result.setText("Select To Date!");
        }else{

            LocalDate startDate = LocalDate.of(fromCalendar.get(Calendar.YEAR), fromCalendar.get(Calendar.MONTH)+1, fromCalendar.get(Calendar.DAY_OF_MONTH));
            LocalDate endDate = LocalDate.of(toCalendar.get(Calendar.YEAR), toCalendar.get(Calendar.MONTH)+1, toCalendar.get(Calendar.DAY_OF_MONTH));

            Period diff = Period.between(startDate, endDate);
            result.clearComposingText();
            result.setText("*****Difference*****\n"+diff.getYears() + " years " + diff.getMonths() + " months " + diff.getDays() + " days.");
        }
    }

}
