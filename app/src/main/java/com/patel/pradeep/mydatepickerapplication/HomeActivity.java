package com.patel.pradeep.mydatepickerapplication;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Intel on 10/7/2017.
 */

public class HomeActivity extends AppCompatActivity {

    TextView home;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        home = (TextView) findViewById(R.id.home);

        //home.setText("HOME ACTIVITY");

    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        home.setText("HOME ACTIVITY");
    }
}
